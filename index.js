import mongoose from 'mongoose';
const { Schema } = mongoose;

const user = new Schema({
	name: { type: String, default: 'hahaha' },
	age: { type: Number, min: 18, index: true },
	dateCreate: { type: Date, default: Date.now },
});

mongoose.connect('mongodb://127.0.0.1/main')
	.then(async () => {
		console.log('Connected!');

		const User = mongoose.model('user', user);

		const userList = await User.find({ age: [ { "$gt": 32 }, { "$lt": 39 }] });

		console.log(userList);

	}).catch( e => {
		console.log("DB not connected", e);
});

